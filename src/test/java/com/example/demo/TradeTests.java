package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class TradeTests {
	
	private long testId;
	private String testStock;
	private int testBuy;
	private double testPrice;
	private int testVolume;
	
/**
 * Test to check that Trade parameters match the arguments
 */
	 	@Test
	    public void test_Trade_Constructor() {
	        Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume );

	        testTrade.setId(testId);
	        testTrade.setStock(testStock);
	        testTrade.setBuy(testBuy);
	        testTrade.setPrice(testPrice);
	        testTrade.setVolume(testVolume);
	        
	        assertEquals(testId, testTrade.getId());
	        assertEquals(testStock, testTrade.getStock());
	        assertEquals(testBuy, testTrade.getBuy());
	        assertEquals(testPrice, testTrade.getPrice(),0.01);
	        
	    }
	 	
/**
 * Test to check that the printed values match the arguments in the constructor
 */
	 	@Test
	    public void test_Trade_toString() {
	        Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

	        assertTrue(testTrade.toString().contains(Long.toString(testId)));
	        assertTrue(testTrade.toString().contains(testStock));
	        assertTrue(testTrade.toString().contains(Integer.toString(testBuy)));
	        assertTrue(testTrade.toString().contains(Double.toString(testPrice)));
	        assertTrue(testTrade.toString().contains(Integer.toString(testVolume)));
	        
	    }

/**
 * Test to see if the two objects created are the same	 	
 */
	 	@Test
	    public void test_Trade_equals() {
	        Trade firstTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
	        Trade compareTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
	        assertFalse(firstTrade == compareTrade);
	        assertTrue(firstTrade.equals(compareTrade));
	    }

}
