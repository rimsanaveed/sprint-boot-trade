package com.example.demo;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
public class TradeControllerTests {
	
	private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeInterface tradeinterface;
    
    @Test
    public void findAllTrades_returnsList() throws Exception {
        when(tradeinterface.findAll()).thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from tradeinterface.findAll: " +
                    result.getResponse().getContentAsString());
    }
    
    @Test
    public void getTradeById_returnsOK() throws Exception {
        Trade testTrade = new Trade(9,"Citigroup", 1, 10, 50);

        when(tradeinterface.findById(testTrade.getId())).thenReturn(
                                                    Optional.of(testTrade));

        MvcResult result = this.mockMvc.perform(get("/trades/" + testTrade.getId()))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from tradeinterface.getStock: " +
                    result.getResponse().getContentAsString());
    }
    
    @Test
    public void deleteById_returnsOK() throws Exception {
        Trade testTrade = new Trade(9,"Citigroup", 1, 10, 50);

        tradeinterface.deleteById(testTrade.getId());
        
    }


}
