package com.example.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {
	
	private static final Logger LOG = LoggerFactory.getLogger(
            TradeControllerIntegrationTests.class);
	
	@Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getTrade_returnsTrade() {
    	long testId = 9;
    	String testStock = "Citigroup";
    	int testBuy = 1;
    	double testPrice = 10;
    	int testVolume = 50;

        ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trades",
                                             new Trade(testId, testStock, testBuy, testPrice, testVolume),
                                             Trade.class);

        LOG.info("Create Trade response: " + createTradeResponse.getBody());
        assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
        assertEquals(testId, createTradeResponse.getBody().getId());
        assertEquals(testStock, createTradeResponse.getBody().getStock());
        assertEquals(testBuy, createTradeResponse.getBody().getBuy());
        assertEquals(testPrice, createTradeResponse.getBody().getPrice(), 0.01);
        assertEquals(testVolume, createTradeResponse.getBody().getVolume());
        
        ResponseEntity<Trade> findTradeResponse = restTemplate.getForEntity(
        		"/trades/" + createTradeResponse.getBody().getId(),Trade.class);
        
        LOG.info("FindById Response: " + findTradeResponse.getBody());
        assertEquals(HttpStatus.OK, findTradeResponse.getStatusCode());
        assertEquals(testId, findTradeResponse.getBody().getId());
        assertEquals(testStock,findTradeResponse.getBody().getStock());
        assertEquals(testBuy,findTradeResponse.getBody().getBuy());
        assertEquals(testPrice,findTradeResponse.getBody().getPrice(), 0.01);
        assertEquals(testVolume,findTradeResponse.getBody().getVolume());

    }
}
