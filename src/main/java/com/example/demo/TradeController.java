package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;


public class TradeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	 	
		@Autowired
	    private TradeInterface tradeinterface;
	 
	 	@RequestMapping(method=RequestMethod.GET)
	    public Iterable<Trade> findAll() {
	        LOG.info("HTTP GET findAll()");
	        return tradeinterface.findAll();
	    }
	    
	    @RequestMapping(value="/{id}", method=RequestMethod.GET)
	    public Trade findById(@PathVariable long id) {
	        LOG.info("HTTP GET findById() id=[" + id + "]");
	        return tradeinterface.findById(id).get();
	    }
	    
	    @RequestMapping(method=RequestMethod.POST)
	    public HttpEntity<Trade> save(@RequestBody Trade trade) {
	        LOG.info("HTTP POST save() stock=[" + trade + "]");
	        return new ResponseEntity<Trade>(tradeinterface.save(trade), 
	        		HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	    @ResponseStatus(value=HttpStatus.NO_CONTENT)
	    public void deleteById(@PathVariable long id) {
	        LOG.info("HTTP DELETE delete() id=[" + id + "]");
	        tradeinterface.deleteById(id);
	    }

}
