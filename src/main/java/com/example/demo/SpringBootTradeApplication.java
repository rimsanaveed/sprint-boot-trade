package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTradeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTradeApplication.class, args);
	}

}
