CREATE TABLE IF NOT EXISTS `trade` (
 id int NOT NULL AUTO_INCREMENT,
 stock varchar(50) NOT NULL,
 buy int NOT NULL,
 price double NOT NULL,
 volume int NOT NULL,
 PRIMARY KEY (`id`)
); 